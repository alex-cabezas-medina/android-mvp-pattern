package com.acabezas.mvp_pattern_sample.control;

/**
 * Created by alexandercabezas.
 */

public interface ControlPresenter {

    void goToLoginScreen();
}
