package com.acabezas.mvp_pattern_sample.control;

import android.app.Activity;
import android.content.Intent;

import com.acabezas.mvp_pattern_sample.login.LoginActivity;

/**
 * Created by alexandercabezas.
 */

public class ControlPresenterImpl implements ControlPresenter{

    private Activity mActivity;


    ControlPresenterImpl(ControlView view) {
        mActivity = (Activity) view;
    }

    @Override
    public void goToLoginScreen() {

        Intent intent = new Intent(mActivity, LoginActivity.class);
        mActivity.startActivity(intent);
    }
}
