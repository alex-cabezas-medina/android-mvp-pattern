package com.acabezas.mvp_pattern_sample.control;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class ControlActivity extends AppCompatActivity implements ControlView{

    ControlPresenter mControlPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mControlPresenter = new ControlPresenterImpl(this);
        mControlPresenter.goToLoginScreen();
    }
}
