package com.acabezas.mvp_pattern_sample.home;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.acabezas.mvp_pattern_sample.R;


public class HomeActivity extends AppCompatActivity implements HomeView {

    HomePresenter mHomePresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mHomePresenter = new HomePresenterImpl();
    }
}
