package com.acabezas.mvp_pattern_sample.login;

import android.app.Activity;
import android.content.Intent;

import com.acabezas.mvp_pattern_sample.R;
import com.acabezas.mvp_pattern_sample.home.HomeActivity;

/**
 * Created by alexandercabezas.
 */

public class LoginPresenterImpl implements LoginPresenter {

    LoginView mView;
    LoginInteractor mLoginInteractor;

    LoginPresenterImpl(LoginView view) {
        mView = view;
        mLoginInteractor = new LoginInteractorImpl();
    }

    @Override
    public void onDestroy() {
        mView = null;
    }

    @Override
    public void onLoginButtonPressed(String userNameText, String passwordText) {
       boolean result = mLoginInteractor.loginUser(userNameText, passwordText);

        if(result) {
            Intent intent = new Intent((Activity)mView, HomeActivity.class);
            ((Activity)mView).startActivity(intent);
        } else {
            mView.onLoginFailed(R.string.login_failed);
        }
    }
}
