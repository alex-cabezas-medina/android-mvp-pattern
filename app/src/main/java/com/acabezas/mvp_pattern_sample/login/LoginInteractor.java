package com.acabezas.mvp_pattern_sample.login;

/**
 * Created by alexandercabezas.
 */

public interface LoginInteractor {

    boolean loginUser(String userName, String password);
}
