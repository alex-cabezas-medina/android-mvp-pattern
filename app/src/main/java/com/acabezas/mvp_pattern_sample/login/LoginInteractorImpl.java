package com.acabezas.mvp_pattern_sample.login;

/**
 * Created by alexandercabezas.
 */

public class LoginInteractorImpl implements LoginInteractor {

    @Override
    public boolean loginUser(String userName, String password) {
        if(userName.equals("username") && password.equals("12345")) {
            return true;
        } else {
            return false;
        }
    }
}
