package com.acabezas.mvp_pattern_sample.login;

/**
 * Created by alexandercabezas.
 */

public interface LoginPresenter {

    void onDestroy();
    void onLoginButtonPressed(String userNameText, String passwordText);
}
