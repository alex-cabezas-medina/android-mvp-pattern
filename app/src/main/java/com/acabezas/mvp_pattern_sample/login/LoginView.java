package com.acabezas.mvp_pattern_sample.login;

/**
 * Created by alexandercabezas on 23/10/17.
 */

public interface LoginView {

    void onLoginFailed(int id);

}
